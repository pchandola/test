FROM tomcat:8.0
COPY tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml
COPY war-app.war /usr/local/tomcat/webapps/hello-world.war
EXPOSE 8888
